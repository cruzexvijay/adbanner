@extends('layouts.app')


@section('sidebar')
     <div id="wrapper">
        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav nav-pills nav-stacked" id="menu">
 
                <li class="active">
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-dashboard fa-stack-1x "></i></span> Dashboard</a>
                       <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="#">link1</a></li>
                        <li><a href="#">link2</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-flag fa-stack-1x "></i></span> Shortcut</a>
                    <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-flag fa-stack-1x "></i></span>link1</a></li>
                        <li><a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-flag fa-stack-1x "></i></span>link2</a></li>
 
                    </ul>
                </li>
                <li>
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-cloud-download fa-stack-1x "></i></span>Overview</a>
                </li>
                <li>
                    <a href="#"> <span class="fa-stack fa-lg pull-left"><i class="fa fa-cart-plus fa-stack-1x "></i></span>Events</a>
                </li>
                <li>
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-youtube-play fa-stack-1x "></i></span>About</a>
                </li>
                <li>
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-wrench fa-stack-1x "></i></span>Services</a>
                </li>
                <li>
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x "></i></span>Contact</a>
                </li>
            </ul>
        </div><!-- /#sidebar-wrapper -->
        <!-- Page Content -->
       
    <!-- /#wrapper -->
    <!-- jQuery -->
<!-- 
    <script src="{{asset('js/sidebar.js')}}"></script> -->
@endsection


@section('content')
<script
src="http://maps.googleapis.com/maps/api/js">
</script>

<script>
function initialize() {
  var mapProp = {
    center:new google.maps.LatLng(51.508742,-0.120850),
    zoom:5,
    mapTypeId:google.maps.MapTypeId.ROADMAP
  };
  var map=new google.maps.Map(document.getElementById("googleMap"), mapProp);
}
google.maps.event.addDomListener(window, 'load', initialize);
</script>


<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="col-sm-12 col-sm-offset-1">
            <div class="panel panel-primary">
                <div class="panel-heading">
                   <h4 class="text-center"> Advertisement Banners Statistics</h4>
                   <h4 class="text-center"><b>Total Banners: 75 </b></h4>
                </div>

                <div class="panel-body"></div>
                 <div class="row">
                         
                        <div class="col-sm-4 col-sm-offset-1">
                            <h3 class="text-center" >Booked : 25</h3>
                                <div class="col-sm-offset-2">
                                    <div class="c100 p33 green">
                                        <span>33%</span>
                                        <div class="slice">
                                            <div class="bar"></div>
                                            <div class="fill"></div>
                                        </div>
                                    </div>
                            </div>
                        </div>   

                        <div class="col-sm-4 col-sm-offset-2">
                            <h3 class="text-center" >Free : 50</h3>
                                <div class="col-sm-offset-2">
                                    <div class="c100 p67 orange">
                                        <span>67%</span>
                                        <div class="slice">
                                            <div class="bar"></div>
                                            <div class="fill"></div>
                                        </div>
                                    </div>
                            </div>
                        </div>      

                    </div>
                </div>
            </div>
        </div>
    
        <div class="col-md-6">
            <div class="col-sm-12 col-sm-offset-1">
            <div class="panel panel-primary">
                <div class="panel-heading">
                   <h4 class="text-center"> Earnings </h4>
                </div>

                <div class="panel-body">
                <div class="row">
                    <div class="col-sm-4 col-sm-offset-1">
                            <div><h3 class="">This Month</h3></div>
                            $
                            <div class="odometer text-success" id="odometer"><b>123</b></div>
                    </div>  

                    <div class="col-sm-4 col-sm-offset-2">
                            <div><h3 class="">Last Month</h3></div>
                            $
                            <div class="odometer text-primary" id="odometer1">123</div>
                     </div> 

                     <div class="col-sm-12">
                        <div class="text-center">Earnings</div>
                     </div> 
                </div>
                   <script type="text/javascript">
                       
                       setTimeout(function(){
                           odometer.innerHTML = 15000;
                       },1000);



                       setTimeout(function(){
                           odometer1.innerHTML = 150000;
                       },1000);

                   </script>
                   <style type="text/css">
                   .odometer{
                    font-size: 30px;
                    text-align: center;
                   }
                   </style>
                </div>
            </div>
        </div>
    </div>
@endsection

