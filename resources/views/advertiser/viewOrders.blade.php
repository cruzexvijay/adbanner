@extends('layouts.layout_advertiser')

@section('viewOrders')

<div class="box box-info">
  <div class="box-header with-border">
    <h3 class="box-title">My Orders</h3>
    
  </div><!-- /.box-header -->
  <div class="box-body">
    <div class="table-responsive">
      <table class="table no-margin glyphicon-hover">
        <thead>
          <tr>
            <th>Order ID</th>
            <th>Item</th>
            <th>Cost</th>
            <th>Status</th>
            <th>Start Date</th>
            <th>End Date</th>
            <th>Options</th>
          </tr>
        </thead>
        <tbody>

        @if(count($orders)>0)

          @foreach ($orders as $order)
            <tr>
              <td><a href="{{ url('') }}/{{Auth::user()->username}}/orders/invoice/{{$order->id}}">OR{{$order->id}}</a></td>
              <td>{{$order->banner->banner_name}}</td>
             
              <td><span class=""><b>${{$order->cost}}</b></span></td>
              
            <td> 

              @if ($order->getOrderStatus() != 0)
                <span class="label label-success">On Going</span>
              @else
                <span class="label label-warning">Expired</span>  
              @endif
            </td>

            <td> {{$order->getFromDate()}} </td>

            <td> {{$order->getTillDate()}} </td>
      
            <td>
      
                <div class="col-md-12">
                  <a href="" class="modal-btn-edit "><span class="fa fa-download" style="font-size: 14px"> invoice</span></a>
                 </div>

            </td>

                                    
            </tr>
           @endforeach
        @else
            <tr >
                <td valign="top" colspan="5" class="dataTables_empty">You have not received any order's. Sit tight</td>
            </tr>
        @endif        
          
        </tbody>
      </table>
    </div><!-- /.table-responsive -->
  </div><!-- /.box-body -->
  
</div><!-- /.box -->

<style type="text/css">
  
  table.glyphicon-hover .fa {
    visibility: hidden;
  }
  table.glyphicon-hover td:hover .fa {
    visibility: visible;
    font-size: 14px;
    
  }


</style>
@endsection