@extends('layouts.layout_advertiser')
  
@section('editForm')



<div class="box box-info">
                    <div class="box-header">
                      <h3 class="box-title" >{{$banner->banner_name}}<h3 class="text-center"></h3></h3>
                      <div class="box-tools pull-right">
<button id="delete-btn" class="btn btn-danger btn-sm"  data-csrf="{{ csrf_token() }}">Delete Banner</button>
                        
                      </div>
                    </div><!-- /.box-header -->
	{{-- expr --}}
   
	            <!-- form start -->

  <form class="form-horizontal" role="form" id="banner_edit" name="banner_edit" method="post" action="{{ url('') }}/banners/update/{{$banner->id}}/">
	             {{method_field('PATCH')}}
		
			@include('advertiser.form', ['buttontext' => 'Update Details','form_type'=>'edit'])
   
	             {{ csrf_field() }}
</form>
	     </div>

<script>

    var lat = document.getElementById("latitude");
    var long = document.getElementById("longitude");

    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        } else { 
            alert("Geolocation is not supported by this browser.");
        }
    }

    function showPosition(position) {
        lat.value = position.coords.latitude;
        long.value=position.coords.longitude;	
    }


 	function showmapPopup(){

 		var baseUrl = '{{url('')}}'+'/{{Auth::user()->username}}'+"/banners/showPopup/banner_edit/";
 		
 		window.open(baseUrl,'Pick a Location',"toolbar=1,location=1,width=500px,height=500px"); 
 	}

  $('#delete-btn').click(function(event){
     
          var name = "{{$banner->banner_name}}";
          var id = {{$banner->id}};
         
          var $textAndPic = $('<div></div>');
                  $textAndPic.append('<br />');
                 
                  $textAndPic.append('<img height="100px" width="100px" src="{{ url('imgs/delete.svg') }}" style="align:center" />');
                  
                  BootstrapDialog.confirm({
                             title: 'Alert ! Are you sure ? ',
                             message: '<h2>Confirm Deletion of ::  '+name+'</h2>',
                             type: BootstrapDialog.TYPE_DANGER, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                            closable: false, // <-- Default value is false
                             draggable: true, // <-- Default value is false
                             btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                             btnOKLabel: 'Delete', // <-- Default value is 'OK',
                             btnOKClass: 'btn-danger', // <-- If you didn't specify it, dialog type will be used,
                             callback: function(result) {
                                 // result will be true if button was click, while it will be false if users close the dialog directly.
                                 if(result) {
                                    sendDeleteRequest(id);
                                 }else {
                                     alert('Good Job');
                                 }
                             }
                         });
                         
                  
         // event.preventDefault();
      });

  function sendDeleteRequest(id) {
    // body...

      var token = $(this).data('csrf');

      var baseUrl = "{{ url('/banners/delete') }}/";

      alert(baseUrl+id+"::"+token);

               $.ajax({
                type:'DELETE',
                url: baseUrl+id,
                data: {_method: 'DELETE',_token:token},
                dataType:'json',
                success:function(response){
                  console.log(response);
                  //parseData(response);
                  alert('success');
                }
                          
          });
  }

 </script>
@stop