@section('latest_orders')
<div class="box box-info">
  <div class="box-header with-border">
    <h3 class="box-title">Latest Orders</h3>
    <div class="box-tools pull-right">
      <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
    </div>
  </div><!-- /.box-header -->
  <div class="box-body">
    <div class="table-responsive">
      <table class="table no-margin">
        <thead>
          <tr>
            <th>Order ID</th>
            <th>Item</th>
            <th>Cost</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>

        @if(count($orders)>0)

          @foreach ($orders as $order)
            <tr>
              <td><a href="">OR{{$order->id}}</a></td>
              <td>{{$order->banner->banner_name}}</td>
             
              <td><span class=""><b>${{$order->cost}}</b></span></td>
              
            <td>

            @if ($order->getOrderStatus() != 0)
              <span class="label label-success">On Going</span>
            @else
              <span class="label label-warning">Expired</span>  
            @endif

            </td>
            </tr>
           @endforeach
        @else
            <tr >
                <td valign="top" colspan="5" class="dataTables_empty">You have not received any order's. Sit tight</td>
            </tr>
        @endif        
          
        </tbody>
      </table>
    </div><!-- /.table-responsive -->
  </div><!-- /.box-body -->
  <div class="box-footer clearfix">
   
    <a href="{{ url('') }}/{{Auth::user()->username}}/orders" class="btn btn-sm btn-default btn-flat pull-right">View All Orders</a>
  </div><!-- /.box-footer -->
</div><!-- /.box -->
@endsection