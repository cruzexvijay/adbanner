
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">	

					<div class="row">
						<h3>Details</h3>
					
					</div>

					<div class="row">
							<br/>
						<div class="col-md-6 col-xs-6">
							<label>Banner ID</label>
						</div>

						<div class="col-md-6">
							<p>{{$banner->id}}</p>
						</div>
					</div>
				
					<div class="row">
						<br/>
						<div class="col-sm-6 col-xs-6">
							<label >Banner Name</label>
						</div>

						<div class="col-md-6">
							<label style="color: blue">{{$banner->banner_name}}</label>
						</div>

					</div>
					<div class="row">
							<br/>
						<div class="col-md-6 col-xs-12">
							<label>Address</label>
						</div>


						<div class="col-md-6">
							<ADDRESS>
								{{$banner->description->address}}
							</ADDRESS>
						</div>

					</div>

					<div class="row">
							<br/>
						<div class="col-md-6 col-xs-12">
							<label>Description</label>
						</div>

						<div class="col-md-6">
							<p>{{$banner->description->description}}</p>
						</div>
					</div>

				

					<div class="row">
						<h3>Specifications</h3>
						<br/>
					</div>

					<div class="row">
						
						<div class="col-md-6 col-xs-6">
							<label>Height</label>
						</div>

						<div class="col-md-6">
							<label>{{$banner->description->dimen_height}}</label><span>   {{$banner->description->dimen_measure}}</span>
						</div>

				  </div>
				
				<div class="row">
						<br/>
					<div class="col-md-6 col-xs-6">
						<label>Width</label>
					</div>

					<div class="col-md-6">
						<label>{{$banner->description->dimen_width}}</label><span>   {{$banner->description->dimen_measure}}</span>
					</div>


				</div>

				<div class="row">
					<h3>Statistics</h3>
					<br/>
				</div>

			<div class="row">
				<div class="col-md-6 col-xs-6">
					<label>Total Orders</label>
				</div>
				
				<div class="col-md-2">
					<label>{{count($banner->order)}}</label>
				</div>
			</div>	

			<div class="row">
					<br/>
			   <div class="col-md-6 col-xs-6">
					<label >Popularity</label>
				</div>

				<div class="col-md-6">
					<div class="progress active">
					  <div class="progress-bar progress-bar-success progress-bar-stripped" style="width: {{$pop}}%" >{{$pop}}%</div>
					</div>
				</div>
			</div>
								
			</div>	
		</div>
	</div>
</body>
</html>
