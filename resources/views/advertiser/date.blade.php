
<!-- HTML Form (wrapped in a .bootstrap-iso div) -->
<!DOCTYPE html>
<html>
<head>
  
  <!-- Special version of Bootstrap that only affects content wrapped in .bootstrap-iso -->
  <link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" /> 

  <!--Font Awesome (added because you use icons in your prepend/append)-->
  <link rel="stylesheet" href="https://formden.com/static/cdn/font-awesome/4.4.0/css/font-awesome.min.css" />

  <!-- Inline CSS based on choices in "Settings" tab -->
  <style>.bootstrap-iso .formden_header h2, .bootstrap-iso .formden_header p, .bootstrap-iso form{font-family: Arial, Helvetica, sans-serif; color: black}.bootstrap-iso form button, .bootstrap-iso form button:hover{color: white !important;} .asteriskField{color: red;}</style>
</head>
<body>

<div class="bootstrap-iso">
 <div class="container-fluid">
  <div class="row">
   <div class="col-md-8 col-sm-8 col-xs-8">
    <form class="form-horizontal" method="post">
     <div class="form-group ">
      <label class="control-label col-sm-2 requiredField" for="name">
       Name
       <span class="asteriskField">
        *
       </span>
      </label>
      <div class="col-sm-10">
       <input class="form-control" id="name" name="name" placeholder="Your Name" type="text"/>
      </div>
     </div>
     <div class="form-group ">
      <label class="control-label col-sm-2 requiredField" for="email">
       Email
       <span class="asteriskField">
        *
       </span>
      </label>
      <div class="col-sm-10">
       <input class="form-control" id="email" name="email" placeholder="Email" type="text"/>
      </div>
     </div>
     <div class="form-group ">
      <label class="control-label col-sm-2 requiredField" for="date">
       Book From
       <span class="asteriskField">
        *
       </span>
      </label>
      <div class="col-sm-4">
       <div class="input-group">
        <input class="form-control" id="date" name="date" placeholder="MM/DD/YYYY" type="text"/>
        <div class="input-group-addon">
         <i class="fa fa-calendar">
         </i>
        </div>
       </div>
      </div>
    
          <label class="control-label col-sm-2 requiredField" for="date1">
       Book Till
       <span class="asteriskField">
        *
       </span>
      </label>
      <div class="col-sm-4">
       <div class="input-group">
        <input class="form-control" id="date1" name="date1" placeholder="MM/DD/YYYY" type="text"/>
        <div class="input-group-addon">
         <i class="fa fa-calendar">
         </i>
        </div>
       </div>
      </div>
     </div>
     <div class="form-group ">
      <label class="control-label col-sm-2 requiredField" for="payment">
       Payment Method
       <span class="asteriskField">
        *
       </span>
      </label>
      <div class="col-sm-10">
       <select class="select form-control" id="payment" name="payment">
        <option value="Select Payment Method">
         Select Payment Method
        </option>
        <option value="Credit/Debit Card">
         Credit/Debit Card
        </option>
        <option value="Net Banking">
         Net Banking
        </option>
        <option value="PayPal">
         PayPal
        </option>
       </select>
      </div>
     </div>
     <div class="form-group">
      <div class="col-sm-10 col-sm-offset-2">
       <button class="btn btn-primary " name="submit" type="submit">
        Submit
       </button>

        <button class="btn btn-primary " type="button" role="button" id="check" name="check">
        check
       </button>

      </div>
     </div>
    </form>
   </div>
  </div>
 </div>
</div>
</body>
</html>

<!-- Extra JavaScript/CSS added manually in "Settings" tab -->
<!-- Include jQuery -->
<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>

<!-- Include Date Range Picker -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

<script>
    $(document).ready(function(){
        var date_input=$('input[name="date"]');
        var date_input1=$('input[name="date1"]');
         //our date input has the name "date"
        var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
        
        $('#date').datepicker({
            format: 'yyyy-mm-dd',
            container: container,
            todayHighlight: true,
            autoclose: true,
        });

        $('#date1').datepicker({
            format: 'yyyy-mm-dd',
            container: container,
            todayHighlight: true,
            autoclose: true,
        })

        $('#check').click(function(e){

            var f  = $('#date').val().split('-');
            var t = $('#date1').val().split('-');

            if(f[2] < t[2] && f[1] <= t[1] ){
              alert("valid");
            }else
            {
              alert('invalid');
            }
        });
    })
</script>

