@section('recently_added')
<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Recently Added Products</h3>
    <div class="box-tools pull-right">
      <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>

    </div>
  </div><!-- /.box-header -->
          

 <div class="box-body">
    <ul class="products-list product-list-in-box">

        @if(count($recents)>0)
           
               @foreach ($recents as $banner )
                 <li class="item">            
                   <div class="">
                   @if($banner->banner_status == '1')
                     <a href="" id="options"  class="product-title dropdown-toggle" data-toggle="dropdown">{{$banner->banner_name}} 

                     <span class="label label-success pull-right">{{$banner->cost}}</span></a>
                     
                   @else
                     <a href="" id="options"  class="product-title dropdown-toggle" data-toggle="dropdown">{{$banner->banner_name}} 

                     <span class="label label-danger pull-right">{{$banner->cost}}</span></a>
                   @endif  
                       <div class="btn-group">
                        <a type="button" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <span class="caret"></span>
                      
                        <span class="sr-only">Toggle Dropdown</span>
                      </a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="javascript:showOnMap({{$banner->location->latitude}},{{$banner->location->longitude}},'{{'<strong>'.$banner->banner_name.'</strong><br/>'.$banner->location->location_description}}');">Show on Map</a></li>
                        <li><a href="{{ url('/banners/edit') }}/{{$banner->id}}">Edit Details</a></li>
                        <li class="divider"></li>
                         <li><a href="#">View Details</a></li>
                      </ul>
                      </div>
                     <span class="product-description">
                       {{$banner->location->location_description}}
                     </span>
                   </div>
                 </li><!-- /.item -->
              @endforeach
          @else
              <p class="text-center">No Banners</p>
          @endif

    </ul>
  </div><!-- /.box-body -->

  <div class="box-footer text-center">
    <a href="javascript::;" class="uppercase">View All Products</a>
  </div><!-- /.box-footer -->
</div><!-- /.box -->
@endsection