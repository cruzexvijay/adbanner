@extends('layouts.layout_advertiser')

@section('maps')
  <script
  src="http://maps.googleapis.com/maps/api/js">
  </script>

  <script>

  var myCenter = new google.maps.LatLng(12.9715987, 77.5945627);
  var title = 'Bengaluru';

  function showOnMap(lat,long,btitle){
      myCenter=new google.maps.LatLng(lat,long);   
      title=btitle

      initialize();      
  }

  
 function initialize()
  {
   // alert(myCenter);

  var mapProp = {
    center:myCenter,
    zoom:18,
    mapTypeId:google.maps.MapTypeId.SATELLITE
    };

  var map=new google.maps.Map(document.getElementById("map-canvas"),mapProp);

  var marker=new google.maps.Marker({
    position:myCenter,
    });

  marker.setMap(map);
  infowindow = new google.maps.InfoWindow({content:title});
  google.maps.event.addListener(marker, 'click', function(){infowindow.open(map,marker);});infowindow.open(map,marker);
  
  }
  google.maps.event.addDomListener(window, 'load', initialize);
  
  </script>
@endsection



@section('monthly_report')
<div class="box">
  <div class="box-header with-border">
    <h3 class="box-title">Monthly Recap Report</h3>
    <div class="box-tools pull-right">
      <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      <div class="btn-group">
        <button class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown"><i class="fa fa-wrench"></i></button>
        <ul class="dropdown-menu" role="menu">
          <li><a href="#">Action</a></li>
          <li><a href="#">Another action</a></li>
          <li><a href="#">Something else here</a></li>
          <li class="divider"></li>
          <li><a href="#">Separated link</a></li>
        </ul>
      </div>
      <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
    </div>
  </div><!-- /.box-header -->
  <div class="box-body" style="display: block;">
    <div class="row">
      <div class="col-md-8">
        <p class="text-center">
          <strong>Sales: 1 Jan, 2014 - 30 Jul, 2014</strong>
        </p>
        <div class="chart">
          <!-- Sales Chart Canvas -->
          <canvas id="salesChart" style="height: 102px; width: 488px;" width="488" height="102"></canvas>
        </div><!-- /.chart-responsive -->
      </div><!-- /.col -->
      <div class="col-md-4">
        <p class="text-center">
          <strong>Statistics</strong>
        </p>
            <div class="row">
                  <div class="col-sm-6 col-md-6 col-xs-6" align="center" >
                     
                         <div style="display:inline;width:100px;height:120px;">
                        
                         <input type="text" class="knob" id="booked-knob"readOnly="true" data-thickness="0.2" data-anglearc="250" data-angleoffset="-125" value="30" data-width="120" data-height="120" data-fgcolor="green" style="width: 64px; height: 40px; position: absolute; vertical-align: middle; margin-top: 40px; margin-left: -92px; border: 0px; font-style: normal; font-variant: normal; font-weight: bold; font-stretch: normal; font-size: 24px; line-height: normal; font-family: Arial; text-align: center; color: rgb(0, 192, 239); padding: 0px; -webkit-appearance: none; background: none;" ></div>
                         <div ><b class="text-center">Booked : {{$stats['booked']}}</b></div>
                      
                      </div>
                     
                        <div class="col-sm-6 col-md-6 col-xs-6" align="center">
                            <div style="display:inline;width:100px;height:120px;">
                          
                            <input type="text" class="knob" id="free-knob"readOnly="true" data-thickness="0.2" data-anglearc="250" data-angleoffset="-125" value="30" data-width="120" data-height="120" data-fgcolor="orange" style="width: 64px; height: 40px; position: absolute; vertical-align: middle; margin-top: 40px; margin-left: -92px; border: 0px; font-style: normal; font-variant: normal; font-weight: bold; font-stretch: normal; font-size: 24px; line-height: normal; font-family: Arial; text-align: center; color: rgb(0, 192, 239); padding: 0px; -webkit-appearance: none; background: none;" disabled></div>
                            <div><b class="text-center">Free : {{$stats['free']}}</b></div>
                          </div>
                  </div>
                       
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- ./box-body -->
  <div class="box-footer" style="display: block;">
    <div class="row">
      <div class="col-sm-3 col-xs-6">
        <div class="description-block border-right">
          <span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 17%</span>
          <h5 class="description-header">$<div class="odometer" id="odometer">0</div></h5>
          <span class="description-text">TOTAL REVENUE</span>
        </div><!-- /.description-block -->
      </div><!-- /.col -->
      <div class="col-sm-3 col-xs-6">
        <div class="description-block border-right">
          <span class="description-percentage text-yellow"><i class="fa fa-caret-left"></i> 0%</span>

          <h5 class="description-header">$<div class="odometer" id="odometer1">0</div></h5>
          <span class="description-text">TOTAL COST OF BANNERS</span>
        </div><!-- /.description-block -->
      </div><!-- /.col -->
      <div class="col-sm-3 col-xs-6">
        <div class="description-block border-right">
          <span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 20%</span>
          <h5 class="description-header">$<div class="odometer" id="odometer2">0</div></h5>
          <span class="description-text">TOTAL PROFIT</span>
        </div><!-- /.description-block -->
      </div><!-- /.col -->
      <div class="col-sm-3 col-xs-6">
        <div class="description-block">
           <br/>
          <h5 class="description-header"><div class="odometer" id="odometer3">0</div></h5>
          <span class="description-text">TOTAL BANNERS</span>
        </div>
      </div>
    </div><!-- /.row -->
  </div><!-- /.box-footer -->

  <script type="text/javascript">
      
      setTimeout(function(){
          odometer.innerHTML = 35210.43 ;
      },1000);



      setTimeout(function(){
          odometer1.innerHTML = {{$stats['cost']}};
      },1000);

       setTimeout(function(){
          odometer2.innerHTML = 24813.53;
      },1000);

       setTimeout(function(){
          odometer3.innerHTML = {{count($banners)}};
      },1000);

       $(function(){

            var totalCount = {{count($banners)}};
            var freeCount = {{$stats['free']}};
            var result ;

            if(totalCount>0){
              result = Math.round((freeCount/totalCount)*100);
            }

            $("#free-knob").val(result).trigger('change').knob({
              'min':0,
              'max':100,
              'format':function(){
                  return result+"%";
              }
             });
        });

       $(function(){

            var totalCount = {{count($banners)}};
            var bookedCount = {{$stats['booked']}};
            var result;

            if(totalCount>0){
              result = Math.round((bookedCount/totalCount)*100);
            }
            
             $("#booked-knob").val(result).trigger('change').knob({
              'min':0,
              'max':100,
              'format':function(){
               return result+"%";

              }
             });
        });

            var totalCount = {{count($banners)}};
            var bookedCount = {{$stats['booked']}};
            var freeCount = {{$stats['free']}};
            var bresult=0;
            var fresult=0;

            if(totalCount>0){
              bresult = Math.round((bookedCount/totalCount)*100);
              fresult = Math.round((freeCount/totalCount)*100);
            }

       $({animatedVal: 0}).animate({animatedVal:bresult}, {
              duration: 1500,
              easing: "swing", 
              step: function() { 
                  $("#booked-knob").val(Math.ceil(this.animatedVal)).trigger("change"); 
              }
           }); 

       $({animatedVal: 0}).animate({animatedVal: fresult }, {
              duration: 1500,
              easing: "swing", 
              step: function() { 
                  $("#free-knob").val(Math.ceil(this.animatedVal)).trigger("change"); 
              }
           }); 

  </script>
  <style type="text/css">
  .odometer{
   font-size: 17px;
   text-align: center;
  }
  </style>
</div><!-- /.box -->
@endsection

@section('my_banners')
<div class="box box-success">
  <div class="box-header with-border">
    <h3 class="box-title">My Banner Locations</h3>
    <div class="box-tools pull-right">
      <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
       <div class="btn-group">
        <button class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown"><i class="fa fa-wrench"></i></button>
        <ul class="dropdown-menu" role="menu">
          <li><a href="#">Add New Banner</a></li>
          <li class="divider"></li>
          <li><a href="{{url('')}}/{{Auth::user()->username}}/banners/">View All Banners</a></li>
        </ul>
      </div>
      <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
    </div>
  </div><!-- /.box-header -->
  <div class="box-body no-padding">
    <div class="row">
      <div class="col-md-8 col-sm-8">
          <div id="googleMap" style="width:100%;height:440px;">
             <div id="map-canvas" style="overflow:hidden;width:100%;height:440px;padding-left: 10px;padding-top: 10px;padding-bottom: 10px">

             </div>
        </div>
      </div><!-- /.col -->
      <div class="col-md-4 col-sm-4">
       <div class="box ">
         <div class="box-header with-border">
           <h3 class="box-title">My Advertisement Banners</h3>
          </div><!-- /.box-header -->
         <div class="box-body">
           <ul class="products-list product-list-in-box ">
           @if(count($banners)>0)
           
               @foreach ($banners as $banner )
                 <li class="item">
                  
                   <div class="">
                   @if($banner->banner_status == '1')
                     <a href="javascript:showOnMap({{$banner->location->latitude}},{{$banner->location->longitude}},'{{'<strong>'.$banner->banner_name.'</strong><br/>'.$banner->location->location_description}}');" class="product-title">{{$banner->banner_name}} <span class="label label-success pull-right">booked</span></a>
                   @else
                     <a href="javascript:showOnMap({{$banner->location->latitude}},{{$banner->location->longitude}},'{{'<strong>'.$banner->banner_name.'</strong><br/><ADDRESS>'.$banner->location->location_description.'</ADDRESS>'}}');" class="product-title">{{$banner->banner_name}} <span class="label label-danger pull-right">Free</span></a>
                   @endif  
                     <span class="product-description">
                       {{$banner->location->location_description}}
                     </span>
                   </div>
                 </li><!-- /.item -->
              @endforeach
          @else
              <p class="text-center">No Banners</p>
          @endif
          
           </ul>
         </div><!-- /.box-body -->
           <div class="box-footer text-center">
           <a href="/{{Auth::user()->username}}/banners" class="uppercase">View All Banners</a>
         </div><!-- /.box-footer -->
      </div>
      <!-- /.col -->
    </div><!-- /.row -->

    </div>

  </div><!-- /.box-body -->

</div><!-- /.box -->
@endsection


@include('advertiser.lastorders',compact('banners'))

@include('advertiser.recents',compact('banners'))