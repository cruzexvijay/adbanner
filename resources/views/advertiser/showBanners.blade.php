@extends('layouts.layout_advertiser')

@section('banners')

<div class="box box-info">
                    <div class="box-header">
                      <h3 class="box-title" >My Advertisement Banners</h3>
                      <div class="box-tools pull-right">
                        <a href="#addNewModal" class="btn btn-primary btn-sm" role="button" data-toggle="modal" data-backdrop="static" >Add New Banner</a>
                        
                      </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                      <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                      <div class="row">

                      <div class="col-sm-6 col-xs-6">
                      <div id="example1_filter" class="dataTables_filter"><label>Search &nbsp;&nbsp;&nbsp;<input type="search" class="form-control input-sm" placeholder="" aria-controls="example1"></label></div>
                      </div>

                       <div class="col-sm-6 col-xs-6">
                      <div class="dataTables_length pull-right" id="example1_length"><label>Show <select name="example1_length" aria-controls="example1" class="form-control input-sm"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div></div>

                      </div>
                      <div class="row">
    <div class="table-responsive">
      <table class="table no-margin glyphicon-hover">
                        <thead>
                          <tr role="row">
                          <th class="text-center" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Id: activate to sort column descending" style="width: 50px;">Id</th>

                          <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Name: activate to sort column ascending" style="width: 300px;">Name</th>

                          <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Address: activate to sort column ascending" style="width: 350px;">Address</th>

                          <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Map: activate to sort column ascending" style="width: 50px;">Map</th>

                          <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="cost: activate to sort column ascending" style="width: 120px;">cost</th>

                          <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Availability: activate to sort column ascending" style="width: 150px;">Availability</th>

                          <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Bookings: activate to sort column ascending" style="width: 200px;">Bookings</th>

                          <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="options: activate to sort column ascending" style="width: 250px;">options</th>

                          </tr>
                        </thead>
                        <tbody>

                        @if (count($banners)>0)

                            @foreach ($banners as $banner)
                              {{-- expr --}}
                                <tr role="row" class="odd">
                                  <td class="text-center">{{$banner->id}}</td>
                                  <td><b style="color: #3c8dbc">{{$banner->banner_name}}</b></td>
                                  <td>
                                    <ADDRESS>
                                      {{$banner->description->address}}
                                    </ADDRESS>
                                  </td>

                                  <td><a class="map-btn" href="{{url('')}}/{{Auth::user()->username}}/showmap/{{$banner->id}}/" role="button" data-desc="{{$banner->banner_name}}" data-addr="{{$banner->description->address}}"><img src="{{url('imgs/maps.png')}}" width="28" height="28" alt="map" title="click to open Map"></a></td>

                                  <td><b>${{$banner->cost}}</b></td>
                                 @if ($banner->description->availability == 1)
                                   {{-- expr --}}
                                   <td class="">
                                   <span class="label label-success" style="font-size: 12px;">Available</span>
                                   </td>
                                 @else
                                   <td class="">
                                   <span class="label label-warning" style="font-size: 12px;">Not Available</span>
                                   </td>
                                 @endif
                                  <td>{{$banner->getBookingDetails()}}</td>
                                    <td>
                                       <div class="col-md-2">
                                         <a href="{{ url('/banners/edit') }}/{{$banner->id}}/" class="modal-btn-edit "><span class="fa fa-edit" style="font-size: 24px"><b></b></span></a>
                                        </div>
                                        <div class="col-md-2">
                                          <a href="{{ url('') }}/{{Auth::user()->username}}/view/{{$banner->id}}" data-name="{{$banner->banner_name}}" class="modal-btn-view"><span class="fa fa-info-circle" style="font-size: 24px"><b></b></span></a>
                                        </div>
                                           
                                  </td>
                                  
                                </tr>
                            @endforeach
                          {{-- expr --}}
                          @else
                              <tr class="odd">
                                  <td valign="top" colspan="5" class="dataTables_empty">You have not added any banners</td>
                              </tr>
                        @endif
                       </tbody>
                        
                      </table></div></div>
                     </div>
                    </div><!-- /.box-body -->
                  </div>
                
<script src='{{url('')}}/plugins/datatables/jquery.dataTables.min.js'></script>

<script src='{{url('')}}/plugins/datatables/dataTables.bootstrap.min.js'></script>

<div class="modal fade" id="addNewModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                 <h4 class="modal-title text-center" id="title_id">Add New Banner</h4>

            </div>
            <div class="modal-body">
                <div class="container">
                		<div class="row">
       						<div class="col-md-9 col-sm-9 col-xs-12">
       							               
                <!-- form start -->
    <form class="form-horizontal" role="form" id="banner_add" name="banner_add" method="post" action="{{url('')}}/{{Auth::user()->username}}/banners/create">
                
     @include('advertiser.form', ['buttontext' => 'Add Banner','form_type'=>'new'])
                   {!! csrf_field() !!}
                </form>
              </div>
        				
    		</div>
        </div>
                    
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <script type="text/javascript">
    
      $('.map-btn').click(function(event){
          var $link = $(this);
          var name =  $(this).data('desc');
          var addr = $(this).data('addr');

          new BootstrapDialog({

              title   :   "<h4>"+name+"</h4>"+"<ADDRESS>"+addr+"</ADDRESS>",
              message :   $('<div>Loading...</div>').load($link.attr('href')),
             
          }).open();

          event.preventDefault();
      });

      $('.modal-btn-view').click(function(event){
          var $link = $(this);
          var name =  $(this).data('name');


          new BootstrapDialog({
              size :BootstrapDialog.WIDE,
              title   :   "<h4>"+name+"</h4>",
              message :   $('<div>Loading...</div>').load($link.attr('href')),
             
          }).open();

          event.preventDefault();
      });

       
    </script>
    
    <script>

    var lat = document.getElementById("latitude");
    var long = document.getElementById("longitude");

    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        } else { 
            alert("Geolocation is not supported by this browser.");
        }
    }

    function showPosition(position) {
        lat.value = position.coords.latitude;
        long.value=position.coords.longitude;	
    }

 </script>

 <script type="text/javascript">
 	
 	function showPopup(){

 		var baseUrl = '{{url('')}}'+'/{{Auth::user()->username}}'+"/banners/showPopup/banner_add/";
 		
 		window.open(baseUrl,'Pick a Location',"toolbar=1,location=1,width=500px,height=500px"); 

 	}

 </script>
  

<style type="text/css">
	html, body,  {
  margin: 0;
  padding: 0;
  height: 100%;
}

</style>
 <script type="text/javascript">

		 	function parseData(response) 
		 	{		
		 			var title = document.getElementById('banner_name').value;
		 		 showMap(response.latitude,response.longitude);
		 		  $(".modal-header #title_id").html(title);
		 		  $('#myMapModal').modal('show');
		 		 	  		
		 	}

		 	function getAjaxResponse(baseUrl) {
		 		// body...
		 		var response="";
		 	  		 
		 	  		 $.ajax({
		 	  		 	type:"GET",
		 	  		 	url: baseUrl,
		 	  		 	dataType:'json',
		 	  		 	success:function(response){
		 	  		 		console.log(response);
		 	  		 		parseData(response);
		 	  		 		
		 	  		 	}
		 	  		 	 	  		 	
				});
		 	}
		 	   	  
 </script>

<style>
th tr {
	text-align: center;

}

.check-toggle{
	padding-top: 20px;
}

</style>

<style type="text/css">

  table.glyphicon-hover .fa {
    visibility: hidden;
  }
  table.glyphicon-hover td:hover .fa {
    visibility: visible;
    font-size: 14px;
    
  }

  .hidden{
    display: none;
  }

  .visible{
    display: inline;
  }

</style>

@endsection

