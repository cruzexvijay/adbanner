
  <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
  <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

	@if ($form_type == 'new')
		{{-- expr --}}
		 <div class="box-body">
			         <div class="form-group {{ $errors->has('br_name') ? ' has-error' : '' }}">
			               <label for="banner_name" class="col-sm-2 control-label">Banner Name</label>
			                <div class="col-sm-10">
			                    <input type="text" class="form-control" id="br_name" name="br_name" placeholder="Banner Name" value="{{old('banner_name')}}">
			                    @if ($errors->has('br_name'))
			                                <span class="help-block">
			                                    <strong>{{ $errors->first('br_name') }}</strong>
			                                </span>
			                            @endif
			                </div>
			        </div>
			        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
			            <label for="address" class="col-sm-2 control-label">Address</label>
			              <div class="col-sm-10">
			                  <textarea class="form-control" rows="5" name="address" id="address" placeholder="Address">{{old('address')}}</textarea>
			                   @if ($errors->has('address'))
			                                <span class="help-block">
			                                    <strong>{{ $errors->first('address') }}</strong>
			                                </span>
			                            @endif
			                  </div>
			                </div>
			                 <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
			                  <label for="opt-desc" class="col-sm-2 control-label">Description</label>
			                  <div class="col-sm-10">
			                    <textarea class="form-control" rows="5" name="description" id="opt-desc" placeholder="Optional Description">{{old('description')}}</textarea>
			                    @if ($errors->has('description'))
			                                <span class="help-block">
			                                    <strong>{{ $errors->first('description') }}</strong>
			                                </span>
			                            @endif
			                  </div>
			                </div>
			                  <div class="form-group{{ $errors->has('height') || $errors->has('width') ? ' has-error' : '' }}">
			                  <label for="dimens" class="col-sm-2 control-label">Dimensions</label>
			                  <div class="col-sm-2">
			                  	
			                    <input type="text" class="form-control" name="height" id="height" placeholder="height" value="{{old('height')}}">
			                    @if ($errors->has('height'))
			                                <span class="help-block">
			                                    <strong>{{ $errors->first('height') }}</strong>
			                                </span>
			                            @endif
			                    </div>
			                    
			                    <div class="col-sm-2">
			                      <input type="text" class="form-control" name="width" id="width" placeholder="width" value="{{old('width')}}">

			                      		@if ($errors->has('width'))
			                                <span class="help-block">
			                                    <strong>{{ $errors->first('width') }}</strong>
			                                </span>
			                            @endif

			                     </div>
			                      <div class="col-sm-2">
			                     <select class="form-control" id="dimen_measure" name="dimen_measure">
			                     	<option value="">select a measurement type</option>
			                     	<option value="mt">meters</option>
			                     	<option value="ft">feet</option>
			                     	
			                     </select>
			                     @if ($errors->has('measurement'))
			                                <span class="help-block">
			                                    <strong>{{ $errors->first('measurement') }}</strong>
			                                </span>
			                            @endif
			                     </div>
			                     <div class="col-sm-2">
			                     <label for="avail-toggle" class="control-label">Availability</label>
			                     </div>

			                     <div class="col-sm-2">

			                    <input type="checkbox" name="avail-toggle" id="avail-toggle" class="form-control" data-toggle="toggle" data-width="120" data-on="Available" data-size="small" data-off="Not Available" data-onstyle="success" data-offstyle="danger">

			                     </div>
			                     
			                  </div>
			               
			           <div class="form-group{{ $errors->has('cost') || $errors->has('latitude') || $errors->has('longitude') ? ' has-error' : '' }}">
			                  <label for="cost" class="col-sm-2 control-label">Cost </label>
			                  <div class="col-sm-3">
			                    <input type="text" class="form-control" name="cost" id="cost" placeholder="cost" value="{{old('cost')}}">
			                    @if ($errors->has('cost'))
			                                <span class="help-block">
			                                    <strong>{{ $errors->first('cost') }}</strong>
			                                </span>
			                            @endif
			                  </div>

			                  <label for="Location" class="col-sm-1 control-label">Location</label>
			                  <div class="col-sm-2">
			                    <input type="text" class="form-control" name="latitude" id="latitude" placeholder="latitude" value="{{old('latitude')}}">
			                    @if ($errors->has('latitude'))
			                                <span class="help-block">
			                                    <strong>{{ $errors->first('latitude') }}</strong>
			                                </span>
			                            @endif
			                  </div>
			                  <div class="col-sm-2">  
			                    <input type="text" class="form-control" name="longitude" id="longitude" placeholder="longitude" value="{{old('longitude')}}">
			                    @if ($errors->has('longitude'))
			                                <span class="help-block">
			                                    <strong>{{ $errors->first('longitude') }}</strong>
			                                </span>
			                            @endif
			                  </div>
			                  <div class="col-sm-1"> 
			                  <a href="javascript:getLocation()" role="button"> 
			                    <img src="{{url('imgs/gps.png')}}" width="28" height="28" alt="map" title="Current Location"></a>
			                    
			                  </div>
			                  <div class="col-sm-1"> 
			                  <a href="javascript:showPopup()" role="button"> 
			                    <img src="{{url('imgs/maps.png')}}" width="28" height="28" alt="map" title="Choose from Map"></a>
			                  </div>

			                  
			            </div>
	@else
		
		 <div class="box-body">
			         <div class="form-group {{ $errors->has('br_name') ? ' has-error' : '' }}">
			               <label for="banner_name" class="col-sm-2 control-label">Banner Name</label>
			                <div class="col-sm-10">
			                    <input type="text" class="form-control" id="br_name" name="br_name" placeholder="Banner Name" value="{{old('banner_name',$banner->banner_name)}}">
			                    @if ($errors->has('br_name'))
			                                <span class="help-block">
			                                    <strong>{{ $errors->first('br_name') }}</strong>
			                                </span>
			                            @endif
			                </div>
			        </div>
			        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
			            <label for="address" class="col-sm-2 control-label">Address</label>
			              <div class="col-sm-10">
			                  <textarea class="form-control" rows="5" name="address" id="address" placeholder="Address">{{old('address',$banner->description->address)}}</textarea>
			                   @if ($errors->has('address'))
			                                <span class="help-block">
			                                    <strong>{{ $errors->first('address') }}</strong>
			                                </span>
			                            @endif
			                  </div>
			                </div>
			                 <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
			                  <label for="opt-desc" class="col-sm-2 control-label">Description</label>
			                  <div class="col-sm-10">
			                    <textarea class="form-control" rows="5" name="description" id="opt-desc" placeholder="Optional Description">{{old('description',$banner->description->description)}}</textarea>
			                    @if ($errors->has('description'))
			                                <span class="help-block">
			                                    <strong>{{ $errors->first('description') }}</strong>
			                                </span>
			                            @endif
			                  </div>
			                </div>
			                  <div class="form-group{{ $errors->has('height') || $errors->has('width') ? ' has-error' : '' }}">
			                  <label for="dimens" class="col-sm-2 control-label">Dimensions</label>
			                  <div class="col-sm-2">
			                  	
			                    <input type="text" class="form-control" name="height" id="height" placeholder="height" value="{{old('height',$banner->description->dimen_height)}}">
			                    @if ($errors->has('height'))
			                                <span class="help-block">
			                                    <strong>{{ $errors->first('height') }}</strong>
			                                </span>
			                            @endif
			                    </div>
			                    
			                    <div class="col-sm-2">
			                      <input type="text" class="form-control" name="width" id="width" placeholder="width" value="{{old('width',$banner->description->dimen_width)}}">

			                      		@if ($errors->has('width'))
			                                <span class="help-block">
			                                    <strong>{{ $errors->first('width') }}</strong>
			                                </span>
			                            @endif

			                     </div>
			                      <div class="col-sm-2">
			                     <select class="form-control" id="dimen_measure" name="dimen_measure">
			                     	<option value="">select a measurement type</option>
			                     	@if($banner->description->dimen_measure == 'mt')
			                     		<option value="mt" selected="selected">meters</option>
			                     		<option value="ft" >feet</option>
			                     	@else
			                     		<option value="mt">meters</option>
			                     		<option value="ft" selected="selected">feet</option>
			                     	@endif
			                     </select>
			                     @if ($errors->has('measurement'))
			                                <span class="help-block">
			                                    <strong>{{ $errors->first('measurement') }}</strong>
			                                </span>
			                            @endif
			                     </div>
			                     <div class="col-sm-2">
			                     <label for="avail-toggle" class="control-label">Availability</label>
			                     </div>

			                     <div class="col-sm-2">


								@if ($banner->description->availability == '1')
									{{-- expr --}}
									<input type="checkbox" name="avail-toggle" id="avail-toggle" class="form-control" data-toggle="toggle" data-width="120" checked data-on="Available" data-size="small" data-off="Not Available" data-onstyle="success" data-offstyle="danger">
								@else
									<input type="checkbox" name="avail-toggle" id="avail-toggle" class="form-control" data-toggle="toggle" data-width="120" data-on="Available" data-size="small" data-off="Not Available" data-onstyle="success" data-offstyle="danger">	
								@endif
			                    

			                     </div>
			                     
			                  </div>
			               
			           <div class="form-group{{ $errors->has('cost') || $errors->has('latitude') || $errors->has('longitude') ? ' has-error' : '' }}">
			                  <label for="cost" class="col-sm-2 control-label">Cost </label>
			                  <div class="col-sm-3">
			                    <input type="text" class="form-control" name="cost" id="cost" placeholder="cost" value="{{old('cost',$banner->cost)}}">
			                    @if ($errors->has('cost'))
			                                <span class="help-block">
			                                    <strong>{{ $errors->first('cost') }}</strong>
			                                </span>
			                            @endif
			                  </div>

			                  <label for="Location" class="col-sm-1 control-label">Location</label>
			                  <div class="col-sm-2">
			                    <input type="text" class="form-control" name="latitude" id="latitude" placeholder="latitude" value="{{old('latitude',$banner->location->latitude)}}">
			                    @if ($errors->has('latitude'))
			                                <span class="help-block">
			                                    <strong>{{ $errors->first('latitude') }}</strong>
			                                </span>
			                            @endif
			                  </div>
			                  <div class="col-sm-2">  
			                    <input type="text" class="form-control" name="longitude" id="longitude" placeholder="longitude" value="{{old('longitude',$banner->location->longitude)}}">
			                    @if ($errors->has('longitude'))
			                                <span class="help-block">
			                                    <strong>{{ $errors->first('longitude') }}</strong>
			                                </span>
			                            @endif
			                  </div>
			                  <div class="col-sm-1"> 
			                  <a href="javascript:getLocation()" role="button"> 
			                    <img src="{{url('imgs/gps.png')}}" width="28" height="28" alt="map" title="Current Location"></a>
			                    
			                  </div>
			                  <div class="col-sm-1"> 
			                  <a href="javascript:showmapPopup()" role="button"> 
			                    <img src="{{url('imgs/maps.png')}}" width="28" height="28" alt="map" title="Choose from Map"></a>
			                  </div>
	                  
			            </div>		
			                  				
	@endif

	                  				
	              </div><!-- /.box-body -->
	              <div class="box-footer">
	                <button type="submit" class="btn btn-primary pull-right">{{$buttontext}} </button>

	                @if ($buttontext != 'Add Banner')
	                	{{-- expr --}}
	                	 <button type="button" class="btn btn-default" id="close-btn" onclick="javascript:window.history.back();"> Cancel</button>
	                @endif
	               
	              </div><!-- /.box-footer -->

