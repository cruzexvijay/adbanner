@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-primary">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if(session()->has('username'))
                        <h4>welcome {{ session()->get('username')}}</h4>
                     @endif
                </div>
              
            </div>
        </div>
    </div>
</div>
@endsection
