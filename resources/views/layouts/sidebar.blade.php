
<section class="sidebar" style="height: auto;">
  <!-- Sidebar user panel -->
  <div class="user-panel">
    <div class="pull-left image">
      <img src="{{ url('') }}{{Auth::user()->getProfilePicture()}}" class="img-circle" alt="User Image" >
    </div>
    <div class="pull-left info">
      <p>{{Auth::user()->username}}</p>
      <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
    </div>
  </div>
  <!-- search form -->
  <form action="#" method="get" class="sidebar-form">
    <div class="input-group">
      <input type="text" name="q" class="form-control" placeholder="Search...">
      <span class="input-group-btn">
        <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
      </span>
    </div>
  </form>
  <!-- /.search form -->
  <!-- sidebar menu: : style can be found in sidebar.less -->
  <ul class="sidebar-menu">
    <li class="header">MAIN NAVIGATION</li>
    <li class="treeview">
      <a href="#">
        <i class="fa fa-dashboard"></i> <span>Dashboard</span> <i class="fa fa-angle-left pull-right"></i>
      </a>
      <ul class="treeview-menu" style="display: none;">
        <li><a href="/{{Auth::user()->username}}"><i class="fa fa-circle-o"></i>Home</a></li>
        <li><a href="#monthly_report"><i class="fa fa-circle-o"></i>Monthly Reports</a></li>
        <li><a href="#my_banners"><i class="fa fa-circle-o"></i>My Banners</a></li>
        <li><a href="#latest_orders"><i class="fa fa-circle-o"></i>Latest Orders</a></li>
        <li><a href="#recently_added"><i class="fa fa-circle-o"></i>Recently Added Banners</a></li>
      </ul>
    </li>

    <li>
      <a href="pages/calendar1.html">
        <i class="fa fa-calendar"></i> <span>Calendar</span>
        <small class="label pull-right bg-red">3</small>
      </a>
    </li>
    <li>
      <a href="pages/mailbox/mailbox1.html">
        <i class="fa fa-envelope"></i> <span>Mailbox</span>
        <small class="label pull-right bg-yellow">12</small>
      </a>
    </li>

     <li>
      <a href="{{url('')}}/{{Auth::user()->username}}/banners/">
        <i class="fa fa-map-o"></i> <span>My Banners</span>
        <small class="label pull-right bg-yellow">{{count(Auth::user()->banner)}}</small>
      </a>
    </li>
    
     <li>
      <a href="{{url('')}}/{{Auth::user()->username}}/orders">
        <i class="fa fa-inbox"></i> <span>My Orders</span>
        <small class="label pull-right bg-yellow">{{count(Auth::user()->getOrderDetails())}}</small>
      </a>
    </li>
    
    
    <li class="header">LABELS</li>
    <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
    <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
    <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>
  </ul>
</section>
