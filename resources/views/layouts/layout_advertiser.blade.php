<html><head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AdminLTE 2 | Dashboard</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="{{url('bootstrap/css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="{{url('plugins/jvectormap/jquery-jvectormap-1.2.2.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{url('dist/css/AdminLTE.min.css')}}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{url('dist/css/skins/_all-skins.min.css')}}">

    <link rel="stylesheet" href="{{url('dist/css/jquery.webui-popover.min.css')}}">

    <link rel="stylesheet" type="text/css" href="{{ elixir('css/app.css') }}">

  <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo asset('vendor/dropzoner/dropzone/dropzone.min.css'); ?>">

  <link rel="stylesheet" type="text/css" href="{{ url('dist/css/croppic.css') }}">

  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.5/css/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />


  <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.3.0/respond.js"></script>

    @yield('maps')
  </head>

  <body class="skin-blue sidebar-mini" style="overflow-x: hidden;">
    <div class="wrapper" >

      <header class="main-header" >

        <!-- Logo -->
        <a href="#" class="logo" style="position: fixed;">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>A</b>BC</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>AdBanner</b>.com</span>
        </a>

        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
            
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="{{ url('') }}{{Auth::user()->getProfilePicture()}}" class="user-image" alt="User Image">
                  <span class="hidden-xs">{{Auth::user()->username}}</span>
                </a>

                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="{{ url('') }}{{Auth::user()->getProfilePicture()}}"class="img-circle" alt="User Image">
                    <p>
                      {{Auth::user()->username}}
                      <small>You are logged in as {{ Auth::user()->type->user_type}}</small>
                    </p>
                  </li>
                  
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="{{ url('') }}/{{Auth::user()->username}}/profile/" class="btn btn-default btn-flat">Profile</a>
                    </div>
                    <div class="pull-right">
                     <!--  <a href="#" class="btn btn-default btn-flat">Sign out</a> -->
                     <a class="btn btn-default btn-flat" href="{{ url('/logout') }}"><i ></i>Logout</a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
              
            </ul>
          </div>

        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar" style="position: fixed;">
        <!-- sidebar: style can be found in sidebar.less -->
        @include('layouts.sidebar')
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper" style="">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          @include('layouts.BreadCrumbs', compact('breadcrumbs','links'))
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Info boxes -->
          <!-- /.row -->
        
        @yield('invoice')
        @yield('profile')
          
          <section name="monthly_report" id="monthly_report">
            <div class="row">
              <div class="col-md-12">
                @yield('monthly_report')
                @yield('banners')
                @yield('editForm')
                @yield('viewOrders')
              </div><!-- /.col -->
            </div><!-- /.row -->
        </section>
         
                  <!-- Main row --> 
        <div class="row">

        <section name="my_banners" id="my_banners"> 
            <!-- Left col -->
            <div class="col-md-12">
              <!-- MAP & BOX PANE -->
                @yield('my_banners')
                @yield('table')
              </div> <!-- column -->
              <!-- /.row -->
        </section>
          
        <section name="latest_orders" id="latest_orders">
          
              <div class="col-md-8">
                  @yield('latest_orders')
            </div><!-- /.col -->

        </section>
              <!-- TABLE: LATEST ORDERS -->
          
          <section name="recently_added" id="recently_added">
            
            <div class="col-md-4">
                         
              <!-- PRODUCT LIST -->
                @yield('recently_added')
            </div><!-- /.col -->
          </section>
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      @include('layouts.footer')
      <!-- Control Sidebar -->
      <!-- /.control-sidebar -->
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
    

    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="{{url('plugins/jQuery/jQuery-2.1.4.min.js')}}"></script>
    
    <!-- Bootstrap 3.3.5 -->
    <script src="{{url('bootstrap/js/bootstrap.min.js')}}"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.5/js/bootstrap-dialog.min.js"></script>
    <!-- FastClick -->
    <script src="{{url('plugins/fastclick/fastclick.min.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{url('dist/js/app.min.js')}}"></script>
    <!-- Sparkline -->
    <script src="{{url('plugins/sparkline/jquery.sparkline.min.js')}}"></script>
    <!-- jvectormap -->
    <script src="{{url('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
    <script src="{{url('plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
    <!-- SlimScroll 1.3.0 -->
    <script src="{{url('plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
    <!-- ChartJS 1.0.1 -->
    <script src="{{url('plugins/chartjs/Chart.min.js')}}"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="{{url('dist/js/pages/dashboard2.js')}}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{url('dist/js/demo.js')}}"></script>
    <script src="http://github.hubspot.com/odometer/odometer.js"></script>
   
    <script src="{{url('plugins/knob/jquery.knob.js')}}"></script>

    <script src="<?php echo asset('vendor/dropzoner/dropzone/dropzone.min.js'); ?>"></script>
    <script src="<?php echo asset('vendor/dropzoner/dropzone/config.js'); ?>"></script>
   

</body></html>