@extends('layouts.layout_advertiser')

@section('profile')
        <!-- Main content -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <section class="content">

          <div class="row">
            <div class="col-md-3">

              <!-- Profile Image -->
              <div class="box box-primary">
                <div class="box-body box-profile">
                  <img class="profile-user-img img-responsive img-circle" src="{{ url('') }}{{$user->getProfilePicture()}}" alt="User profile picture" >
                  <h3 class="profile-username text-center">{{$user->username}}</h3>
                  <p class="text-muted text-center">{{$user->type->user_type}}</p>

                  <ul class="list-group list-group-unbordered">
                    <li class="list-group-item">
                      <b>Name</b> <a class="pull-right">{{$user->name}}</a>
                    </li>

                      <li class="list-group-item">
                      <b>User Name</b> <a class="pull-right">{{$user->username}}</a>
                    </li>

                    <li class="list-group-item">
                      <b>Email Address </b> <a class="pull-right">{{$user->email}}</a>
                    </li>
                   
                  </ul>

                  <a href="#edit" class="btn btn-primary btn-block" data-toggle="tab" ><b>Edit Profile</b></a>

                </div><!-- /.box-body -->
              </div><!-- /.box -->

              <!-- About Me Box -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">About Me</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <strong><i class="fa fa-book margin-r-5"></i>  Organisation</strong>
                  <p class="text-muted pull-right">
                   {{$user->profile->org_name}}
                  </p>

                  <hr>

                  <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>
                  <p class="text-muted pull-right">{{$user->profile->location}}</p>

                  <hr>

                  <strong><i class="fa fa-phone margin-r-5"></i> Phone Number</strong>
                  <p class="text-muted pull-right">{{$user->profile->ph_no}}</p>

                  <hr>

                  <strong><i class="fa fa-file-text  margin-r-5"></i> Address</strong>
                  <ADDRESS class="text-muted pull-right">{{$user->profile->address}}</ADDRESS>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
            <div class="col-md-9">
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#activity" data-toggle="tab">Activity</a></li>
                
                  <li><a href="#edit" data-toggle="tab">Edit Profile</a></li>
                </ul>
                <div class="tab-content">
                  <div class="active tab-pane" id="activity">
                      <label class="text-center">You have not performed any activity yet</label>
                    </div>


                  <div class="tab-pane" id="edit">
                    <div class="row">
                    
                      <div class="col-md-8">
                        <form class="form-horizontal" method="post" action="{{url('')}}/{{Auth::user()->username}}/profile/update/" >
                             {{method_field('PATCH')}}

                          <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Name</label>
                            <div class="col-sm-10">
                              <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="{{old('name',$user->name)}}">
                            </div>
                          </div>
                         
                          <div class="form-group">
                            <label for="org_name" class="col-sm-2 control-label">Organization</label>
                            <div class="col-sm-10">
                              <input type="text" class="form-control" name="org_name" id="org_name" placeholder="Organization" value="{{old('org_name',$user->profile->org_name)}}">
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="address" class="col-sm-2 control-label">Address</label>
                            <div class="col-sm-10">
                              <textarea class="form-control" id="address" name="address" placeholder="Address" rows="5">{{old('address',$user->profile->address)}}</textarea>
                            </div>
                          </div>

                          <div class="form-group">
                            <label for="location" class="col-sm-2 control-label">Location</label>
                            <div class="col-sm-10">
                              <input type="text" class="form-control" id="location" name="location" placeholder="Location" value="{{old('location',$user->profile->location)}}">
                            </div>
                          </div>

                          <div class="form-group">
                            <label for="ph_no" class="col-sm-2 control-label">Contact Number</label>
                            <div class="col-sm-10">
                              <input type="text" class="form-control" id="ph_no" name="ph_no" 
                              placeholder="Contact Number" value="{{old('ph_no',$user->profile->ph_no)}}">
                            </div>
                          </div>
                                  <input type="hidden" name="_token" value="{{csrf_token()}}"></input>
                          <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                              <button type="submit" class="btn btn-danger">Submit</button>
                            </div>
                          </div>

                        </form>
                      </div>  
                      <div class="col-md-4">
                        <div class="col-lg-12 ">
                          <h4 class="centered"> Profile Picture </h4>
                          <p class="centered">( Upload only one picture of max 2MB size)</p>
                          <div id="cropContainerEyecandy">
                                @include('dropzoner::dropzone',['user'=>Auth::user(),'toggle'=>'true'])
                          </div>
                        </div>
                      </div>
    
                    </div>
                  </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
              </div><!-- /.nav-tabs-custom -->
            </div><!-- /.col -->
          </div><!-- /.row -->

          {{var_dump($errors)}}

        </section><!-- /.content -->
  {{-- expr --}}

@stop
  