	<h1>
	  {{$breadcrumbs['name']}}
	  <small>{{$breadcrumbs['subs']}}</small>
	</h1>
	<ol class="breadcrumb">
	  <li><a href="{{url('')}}/{{Auth::user()->username}}/"><i class="fa fa-dashboard"></i> Home</a></li>

	@if (count($links) > 0)
		 @foreach ($links as $link)
	  		<li>{{$link}}</li>
	  @endforeach
	@endif
	 
	  
	</ol>
