<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannerLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banner_locations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('banner_id')->unsigned()->index();
            $table->string('latitude');
            $table->string('longitude');
            $table->string('location_description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('banner_locations');
    }
}
