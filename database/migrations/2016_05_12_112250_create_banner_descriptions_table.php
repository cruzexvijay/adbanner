<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannerDescriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banner_descriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('banner_id')->unsigned()->index();
            $table->string('address');
            $table->string('description');
            $table->integer('dimen_height')->unsigned();
            $table->integer('dimen_width')->unsigned();
            $table->string('dimen_measure');
            $table->integer('availability')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('banner_descriptions');
    }
}
