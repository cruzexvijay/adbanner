<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use App\UserType;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','username', 'email', 'password','user_type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
    * Relationship method defining a user and the type of user.
    */
    public function type()
    {
        # code...
        return $this->belongsTo(UserType::class,'id');//returns a usertype object
    }

    public function banner()
    {
        return $this->hasMany(adBanner::class,'user_id');
    }

    public function order()
    {
        # code...

        return $this->hasMany(Orders::class);
    }

    public function profile()
    {
      # code...
        return $this->hasOne(UserProfile::class,'user_id');
    }

    public function accounts()
    {
      # code...
      return $this->hasMany(AccountDetails::class,'user_id');
    }


    public function pictures()
    {
      # code...
      return $this->hasOne(Images::class,'user_id');
    }

    public function getProfilePicture()
    {
      # code...
      # 
      if($this->pictures != null){

          return $this->pictures->img_url;  
      }

      else
      {
        return '/imgs/user.png';
      }
      
    }
    

    public function getOrderDetails()
    {
        # code...
        $user = $this;

        $ordersArray = [];

        if(count($user->order)==0)
        {
         
            $banners = $user->banner;

            foreach ($banners as $banner) {

                if($banner->order != null){
                        
                        $orders = $banner->order;

                        if(count($orders) > 0){

                            foreach ($orders as $order) {
                                # code...
                                array_push($ordersArray,$order);
                            }
                        
                       }       
                 }else{
                  return 0;
                 }
            }
                asort($ordersArray);  
             return $ordersArray;   
        }
        else{
          
            return array($user->order);
                
        }
           
    }
   
}
