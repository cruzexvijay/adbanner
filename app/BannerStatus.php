<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BannerStatus extends Model
{
    //
    protected $fillable = ['banner_status'];

    public function banner()
    {
    	# code...

    	return $this->hasMany(adBanner::class,'banner_status');
    }

}
