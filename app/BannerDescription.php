<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BannerDescription extends Model
{
    //

    protected $fillable = 
    [
    'banner_id','address','description','dimen_height','dimen_width','dimen_measure','availability',
    ];

    public function banner()
    {
    	# code...

    	return $this->belongsTo(adBanner::class,'banner_id');
    }
}
