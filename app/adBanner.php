<?php

namespace App;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use \DateTime;

class adBanner extends Model
{
    //
    protected $fillable = ['user_id','banner_name','banner_status','cost','location_id'];

    public function user()
    {
    	# code...

    	return $this->belongsTo(User::class,'user_id');
    }

    public function location()
    {
    	# code...

    	return $this->hasOne(BannerLocation::class,'banner_id');
    }


    public function status()
    {
        # code...
        return $this->hasOne(BannerStatus::class,'id');//returns a usertype object
    }

    public function description(){
        return $this->hasOne(BannerDescription::class,'banner_id');
    }

    public function pictures()
    {
        # code...
        # 
        return $this->hasMany(Images::class,'banner_id');
    }
    

    public function addNewBanner(Request $request)
    {
        # code...

        $location = new BannerLocation;
        $desc = new BannerDescription;

        $location->latitude = $request->get('latitude');
        $location->longitude = $request->get('longitude');
        $location->location_description = $request->get('address');
        $location->save();
     
        $desc->address = $request->get('address');
        $desc->description = $request->get('description');
        $desc->dimen_height = $request->get('height');
        $desc->dimen_width = $request->get('width');
        $desc->dimen_measure = $request->get('dimen_measure');
        $desc->save();
        
        if($request->get('avail-toggle')=='on'){
            $desc->availability = 1;
        }else{
            $desc->availability = 0;
        }

        $this->banner_name = $request->get('br_name');
        $this->banner_status =2;
        $this->cost = $request->get('cost');
        $this->save();
        
        $this->location()->save($location);
        $this->description()->save($desc);

        return $this;

    }
    
    public function order()
    {
        # code...
       return $this->hasMany(Orders::class,'banner_id');
    }


    /**
     * Get the booking details.
     *
     * @return     string  Booking details.
     */

    public function getBookingDetails()
    {
        # code...
        $user = $this->user;

        if(count($user->order) == 0)
        {

             $banner = $this->order()->orderBy('created_at','desc')->first();

                     if($banner == null)
                        {
                                 return "Free \n";
                         }
                    else
                        {
                            $till_date = $banner->book_till;
                            return $this->getStatus($till_date);
                        }                    
        }
    
                
    }

    public function getStatus($till_date)
    {
        # code...
        
        $date = new DateTime();
        $cur_date = $date->format('Y-m-d H:i:s');
        
        if(strtotime($cur_date) < strtotime($till_date))
        {
            $this->update(['banner_status'=>1]);
            return "Booked till ".date("j F", strtotime($till_date))."\n";
        }
        else if(strtotime($cur_date)==strtotime($till_date))
        {
            return "expires today\n";
        }
        else
        {
            $this->update(['banner_status'=>2]);

            return "expired\n";
        }
    }    

}
