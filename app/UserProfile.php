<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    //
   protected $fillable = ['user_id','org_name','address','location','ph_no'];

   public function user()
   {
   	# code...
   		return $this->belongsTo(User::class,'user_id');
   }


   

}
