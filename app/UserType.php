<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserType extends Model
{
    //

	protected $fillable = ['user_type'];

	/**
	* Method to define the relationship between the user and the user's type.
	*/
	public function users()
	{
		# code...
		// Many users can be of the same user type.
		//returns the collection of user object sharing the same user type.

		return $this->hasMany(User::class,'user_type'); 
	}


}
