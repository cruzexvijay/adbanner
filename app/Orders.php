<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \DateTime;

class Orders extends Model
{
    //

    protected $fillable = ['user_id','banner_id','cost','book_from','book_till'];

    public function banner()
    {
    	# code...
    	return $this->belongsTo(adBanner::class,'banner_id');
    
    }

    public function user()
    {
    	# code...

    	return $this->belongsTo(User::class,'user_id');
    }

    public function getOrderStatus()
    {
        # code...
        $till_date = $this->book_till;
        $date = new DateTime();

        $cur_date = $date->format('Y-m-d H:i:s');
        
        if(strtotime($cur_date) <= strtotime($till_date))
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }

    public function getFromDate()
    {
        # code...
       return date("jS F, Y", strtotime($this->book_from));
    }


    public function getTillDate()
    {
        # code...
        return date("jS F, Y", strtotime($this->book_till));
    }
}
