<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BannerLocation extends Model
{
    //
    protected $fillable = ['banner_id','latitude','longitude','location_description'];

    public function banner()
    {
    	# code...

    	return $this->belongsTo(adBanner::class,'banner_id');
    }

    
}
