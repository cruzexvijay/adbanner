<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/**
* Note:: From Laravel 5.2, All routes have been redirected through the 'web' middleware group by 
* default. No need to specify them here.
*/

/*
|--------------------------------------------------------------------------
|							 User Routes
|--------------------------------------------------------------------------
|
| 				Routes related to all user functionalities
*/

Route::get('/', function () {
    return view('welcome');
});

Route::auth(); //use the Illuminate\Router.php's default auth() routes

Route::get('/administrator/home', 'HomeController@showAdmin'); 

Route::get('/{username}','HomeController@showUser');

Route::get('/{username}/banners','HomeController@showUserBanners');



/*
|--------------------------------------------------------------------------
|							 User Profile Routes
|--------------------------------------------------------------------------
|
| 				Routes related to all user profile functionalities
*/

Route::get('/{username}/profile','ProfileController@showProfile');

Route::post('/{username}/profile/create','ProfileController@create');

Route::patch('/{username}/profile/update','ProfileController@update');



/*
|--------------------------------------------------------------------------
|							 Banner Routes
|--------------------------------------------------------------------------
|
| 				Routes related to all advertisement banner functionalities
*/

Route::get('/{username}/banners/showPopup/{form}/',function($username,$form){
	return view('advertiser.popup',['form_name'=>$form]);
});

Route::get('/{username}/showmap/{banner}/','BannerController@showMap');

Route::post('/{username}/banners/create','BannerController@addBanner');

Route::get('/banners/edit/{banner}/','BannerController@edit');

Route::get('/{username}/view/{banner}','HomeController@showDetails');

Route::patch('/banners/update/{banner}','BannerController@update');

Route::delete('/banners/delete/{banner}','BannerController@destroy');

/*
|--------------------------------------------------------------------------
|							 Orders Routes
|--------------------------------------------------------------------------
|
| 				Routes related to all orders functionalities
*/

Route::get('/{username}/orders/','HomeController@showOrders');

Route::get('/{username}/orders/invoice/{order}','BannerController@showInvoice');

