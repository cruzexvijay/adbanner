<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

use \Illuminate\Support\Facades\Response;
use \File;
use Validator;
use App\UserProfile;

class ProfileController extends Controller
{
    //
    //
    public function showProfile($username)
    {
    	# code...
    	
    	$breadcrumbs = ['name'=>'User Profile','subs'=>''];

    	  $links = ['User','Profile'];

        $user = Auth::user();

        if($user->profile != null){

    	  $user = $user->load('profile');

    	  return view('layouts.profile',compact('breadcrumbs','links','user'));
        }
        else{
          return view('profiles.newProfile',compact('breadcrumbs','links','user'));
        }

    }

    public function update($username,Request $request)    {
        # code...
        # 
        
        $rules = [
                   'name' => 'required|max:255',
                   'org_name'=>'required|max:255',
                   'address'=>'required|max:255',
                   'location' => 'required|max:255',
                   'ph_no'=>'required|numeric',
                 ];

                 $this->validate($request,$rules);
  
          $user = Auth::user()->load('profile');
      
          $userParams = [
                            'name' => $request->get('name'),
                        ];

         $profileParams = [
                            'org_name' => $request->get('org_name'),
                            'address' => $request->get('address'),
                            'location' => $request->get('location'),
                          ];


            $user->update($userParams);

            $user->profile()->update($profileParams);

            $user->profile()->update(['ph_no'=>$request->get('ph_no')]);

            return back();

    }

    public function create($username,Request $request)
    {
      # code...
      # 
          
          $rules = [
                     'name' => 'required|max:255',
                     'org_name'=>'required|max:255',
                     'address'=>'required|max:255',
                     'location' => 'required|max:255',
                     'ph_no'=>'required|numeric',
                   ];

                   $this->validate($request,$rules);

         $userParams = [
                           'name' => $request->get('name'),
                       ];

        $profileParams = [
                           'org_name' => $request->get('org_name'),
                           'address' => $request->get('address'),
                           'location' => $request->get('location'),
                           'ph_no' => $request->get('ph_no'),
                         ];


          $user = Auth::user();
          
          $profile = new UserProfile;
          
              $user->update($userParams);

              $user->profile()->create($profileParams);

             $url = '/'.Auth::user()->username.'/profile/';

             return redirect($url);
    }
  
}
