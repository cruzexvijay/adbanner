<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\adBanner;
use \DateTime;

class HomeController extends Controller
{


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth'); //use the Middleware/Authenticate middleware for processing the request
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    /**
    * Method to get all the banners associated with the current logged in 
    * @return all banner objects of the user
    */ 
    public function getAllBanners()
    {
        # code...
        $banners = Auth::user()->banner;
        $banners->load('description','location');

        return $banners;
    }

    /**
    * method to display the recently added banners of the user.
    * @return last 5 banner objects
    */

    public function getRecentlyAddedBanners(){

        return adBanner::where('user_id',Auth::user()->id)->orderBy('id','desc')->take(5)->with('description','location')->get();
    }

    /**
    * method to find the total cost of all the banners added by the user
    * @return the summation of all the banner costs;
    */
    public function findTotalCostofAllBanners()
    {
        # code...

        return adBanner::sum('cost');
    }

    public function getUser()
    {
        # code...
        return Auth::user();
    }

    /**
    * method to get the banner stats;
    */
    public function getBannerStatistics()
    {
        # code...
        $booked = count($this->getUser()->banner->where('banner_status','1'));
        $free = count($this->getUser()->banner->where('banner_status','2'));
        $cost = $this->findTotalCostofAllBanners();

        return array("booked"=>$booked,"free"=>$free,"cost"=>$cost);
    }

    /**
    * Methd to show the administrator's home page
    * @return : The view to show the user after logging in as administrator
    **/

    public function showAdmin()
    {
      $breadcrumbs = ['name'=>'DashBoard','subs'=>''];

        $links = ['My Banners'];
        # code...
        session()->flash('username','administrator'); //flash admin's name to the session
        return view('admin.home',compact('breadcrumbs','links')); //return admin/home.blade.php view
    }

     /** 
    * Methd to show the user's home page
    * @param : $username => Name of the logged in user
    * @return : The view to show the user after logging in
    */

    public function showUser($username)
    {
        # code...
         //flashes the username to the session
        //to be used in the home.blade.php view
        session()->flash('username',$username);

        $banners = $this->getAllBanners(); 
        $recents = $this->getRecentlyAddedBanners();
        $orders = $this->getRecentOrders();

        $stats = $this->getBannerStatistics();

        $breadcrumbs = ['name'=>'DashBoard','subs'=>''];

        $links = ['Dashboard'];
      
        return view('advertiser.home',compact('banners','recents','stats','orders','breadcrumbs','links'));
    }

/**
 * Method to display all the banners under the section "My Banners"
 *
 * @param      <String>  $username  username of the logged in user
 *
 * @return     <View>  returns the blade view with inputs as an array of all the banner objects of the advertiser
 */

    public function showUserBanners($username)
    {
        # code...
    $breadcrumbs = ['name'=>'My Banners','subs'=>''];

        $links = ['My Banners'];

        $banners = $this->getAllBanners();
        return view('advertiser.showBanners',compact('banners','breadcrumbs','links'));
    }

/**
 * Get the order history.
 * method to get all the orders of the advertiser's banners
 * @return     array  Order history.
*/
    public function getOrderHistory()
    {
        # code...
        $ordersArray = [];

        $user = Auth::user();
   
        if($user->type->user_type=='customer'){
            return $user->orders;
        }
        else{
                 return $user->getOrderDetails();
        }
       
    }

    /**
     * Get the recent orders.
     *
     * @return     array  an array of Recent orders.
     */
    public function getRecentOrders()
    {
        # code...
        
        if($this->getUser()->type->user_type == 'customer'){

            return $this->getUser()->orders;

        }else{
             $orders = array_slice($this->getOrderHistory(), -4);

        return array_reverse($orders);
        }
       
    }


    /**
     * function to display the details of the selected banner
     *
     * @param      <string>  $username  (Name of the logged in user)
     * @param      <Object>  $id        id of the selected banner
     *
     * @return     string  viewDetails view if the banner exists under the user else returns a 404 message
     */
    public function showDetails($username,$id)
    {
        # code...
        if($banner = adBanner::find($id)){

            $bannerCount = count($this->getOrderHistory());
            $thisBannerCount  = count($banner->order);
            $pop;

            if($bannerCount != 0){
                $pop = round(($thisBannerCount/$bannerCount)*100);
            }else{
                $pop = 0;
            }

            return view('advertiser.viewDetails',['banner'=>$banner,'pop'=>$pop]);
       
        }
        else{
            return view('errors.404');
        }
    }

   public function getBookingDetails($username,$id)
   {
       # code...
       $banner = adBanner::find($id);

       if($banner != null){
            return $banner->getBookingDetails();
        }else{
            return view('errors.404');
        }
   }

   public function showOrders($username)
   {
     # code...
     $orders = Auth::user()->getOrderDetails();

   $breadcrumbs = ['name'=>'View Orders','subs'=>''];

        $links = ['View Orders'];

     return view('advertiser.viewOrders',compact('orders','breadcrumbs','links'));
   }

}

