<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use App\adBanner;
use App\BannerLocation;
use App\BannerDescription;
use App\Orders;
use DB;

use Illuminate\Support\Facades\Auth;

class BannerController extends Controller
{
    //

    public function addBanner($username,Request $request)
    {
    	# code...

        $rules = [
                    'br_name' => 'required|max:255',
                    'address' => 'required|max:255',
                    'description' => 'max:255',
                    'height' => 'required|numeric|min:2',
                    'width' => 'required|numeric|min:2',
                    'dimen_measure'=>'required|in:mt,ft',
                    'cost' => 'required|numeric',
                    'latitude' => 'required|min:1|numeric',
                    'longitude' => 'required|min:1|numeric',
                 ];

    	$this->validate($request,$rules);
    	
    	$banner = new adBanner;

        $user = Auth::user();

       $user->banner()->save($banner->addNewBanner($request));

        return back();

       //return $request->all();
    }


    public function showMap($username,$bannerId)
    {
        $banner = adBanner::find($bannerId);
        # code...
        if($banner->user == Auth::user()){
            $lat = $banner->location->latitude;
            $lng = $banner->location->longitude;
            $title = $banner->location->location_description;

            $data = array('lat'=>$lat,'lng'=>$lng,'title'=>$title);

            return view('advertiser.showmap',compact('data'));
        }else{
            return "<h1>404</h1><h3> You dont have permission to view this page</h4>";
        }

        //return $bannerId;
    }
   
   public function show(adBanner $banner)
   {
       # code...
         $banner->load('description','location');

         return $banner;
   }


   /**
    * method to update the details of the banner using a REST patch request
    *
    * @param      adBanner  $banner   an adBanner model object
    * @param      Request   $request  an object of the Request facade containing the submitted form values to be updated;
    *
    * @return     a url to redirect 
    */
   public function update(adBanner $banner,Request $request)
   {
       # code...
            $rules = [
                    'br_name' => 'required|max:255',
                    'address' => 'required|max:255',
                    'description' => 'max:255',
                    'height' => 'required|numeric|min:2',
                    'width' => 'required|numeric|min:2',
                    'dimen_measure'=>'required|in:mt,ft',
                    'cost' => 'required|numeric',
                    'latitude' => 'required|min:1|numeric',
                    'longitude' => 'required|min:1|numeric',
                 ];

        $this->validate($request,$rules);

       $banner->load('description','location');


       $avail;

      if($request->get('avail-toggle')=='on')
      {
          $avail = 1;
      }
      else
      {
          $avail = 0;
      }

       $params = [
                    'dimen_height' => $request->get('height'),
                    'dimen_width' => $request->get('width'),
                    'availability' => $avail,
                 ];


      $banner->description()->update($request->only('address','description','dimen_measure'));

      $banner->description()->update($params);

      $params = [
              'latitude' => $request->get('latitude'),
              'longitude' => $request->get('longitude'),
              'location_description' => $request->get('address'),  
                ];

      $banner->location()->update($params);

      $banner->update(['banner_name'=>$request->get('br_name'),'cost'=>$request->get('cost')]);


        $url = '/'.Auth::user()->username.'/banners/';

        return redirect($url);

   }

   /**
    * function to display the edit details page of a banner
    *
    * @param      variable  $id     unique ad banner identifier
    *
    * @return     Blade View   returns a edit view if the banner belongs to the logged in user else returns a 404 not found view.
    */

   public function edit($id)
   {

      $br = adBanner::find($id);

      if($br->user == Auth::user()){

        $breadcrumbs = ['name'=>'Edit Details','subs'=>''];
        $links = ['My Banners','Edit'];

        $banner = $br->load('description','location');

        return view('advertiser.edit')->with(compact('banner','breadcrumbs','links'));
      }
      else
      {
        return view('errors.404');
      }
   }

   public function destroy($id,Request $request)
   {
     # code...

        return $request->all();
   }



  /**
   * function to display the order invoice
   *
   * @param      String  $username  name of the logged in user;
   * @param      Orders  $order     object of the model Orders;
   *
   * @return     Blade View  invoice blade view with breadcrum links
   */

  public function showInvoice($username,Orders $order)
  {
    # code...
    # 
    $breadcrumbs = ['name'=>'Invoice','subs'=>'#007612'];
    $links = ['View Orders','Invoice'];

    return view('layouts.invoice',compact('breadcrumbs','links'));
  }

}
