<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
  
    protected $redirectTo = '/';
   
    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    /*Method to redirect the users based on their name*/

    public function getRedirectPath()
    {
        # code...

       if($this->isAdmin(Auth::user()))
        {
            return 'administrator/home';
        }
        else
        {
                $username = Auth::user()->username;//Auth::user() returns the logged in user object.
                return $username; // returns the username of the logged in user
        }

        
    }

    /**
    * Method to check if the user is Administrator
    **/

    public function isAdmin(User $user)
    {
        # code...
       // return $type;

        if($user->type->user_type=="administrator"){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'username' => 'required|min:6|unique:users',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'user_type' => 'required|in:1,2',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
    
        $user_type = $data['user_type'];

        return User::create([
            'name' => $data['name'],
            'username' => $data['username'],
            'user_type' => $user_type,
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);

    }
}
