<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Images extends Model
{
    //
    protected $fillable = ['user_id','banner_id','type','img_url'];

    public function user()
    {
    	# code...
    	return $this->belongsTo(User::class,'user_id');
    }

    public function banner()
    {
    	# code...
    	return $this->belongsTo(adBanner::class,'banner_id');
    }

    

}
